<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>view - Users</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo base_url()."assets/css/styles.css" ?>">
    </head>
    <body>
		<div class="container">
			<div class="header text-center">
				<h1>Usuarios</h1>
				<h2>Modulo de usuarios</h2>
				<p>Módulo de usuairos</p>

				<div id="menuRecord" class="col-12 text-center">
					<a href="#" id="new-record" class="btn btn-secondary">Nuevo</a>
				</div>

					<div id="form">
						<?php 
							if (!empty($record)) {
								$row = $record->row();
								$id=$row->id;
								$usuario=$row->usuario;
								$nombre=$row->nombre;
								$correo=$row->correo;
								$clave=$row->clave;
								$foto=$row->foto;
							}else{
								$id="";
								$usuario="";
								$nombre="";
								$correo="";
								$clave="";
								$foto="";
							}
							echo form_open(base_url().'index.php/Start/save');
						 ?>
						 <div class="row mt-2">
						 	<div class="col-2">
						 		Usuario
						 	</div>
						 	<div class="col-2">
						 		Nombre
						 	</div>
						 	<div class="col-2">
						 		Correo
						 	</div>
						 	<div class="col-2">
						 		Clave
						 	</div>
							 <div class="col-2">
						 		Confirmar Clave
						 	</div>
						 	<div class="col-2">
						 		foto
						 	</div>
						 </div>
						 <div class="row mt-2">
						 	<div class="col-2">
								 <?php 
								 	echo form_hidden('id', $id);
									 
        							echo form_input('usuario',$usuario);
						 		 ?>
						 	</div>
						 	<div class="col-2">
						 		<?php 
        							echo form_input('nombre',$nombre);
						 		 ?>
						 	</div>
						 	<div class="col-2">
						 		<?php 
        							echo form_input('correo',$correo);
						 		 ?>
						 	</div>
						 	<div class="col-2">
						 		<?php 
        							echo form_input('clave',$clave);
						 		 ?>
						 	</div>
							 <div class="col-2">
						 		<?php 
        							echo form_password('claveConfirm');
						 		 ?>
						 	</div>
						 	<div class="col-2">
						 		<?php 
        							echo form_input('foto',$foto);
						 		 ?>
						 	</div>
						 </div>
						 <div class="row mt-2 mb-2">
						 	<div class="col-6 text-right">
						 		<?php 
						 			$claseBtnSubmit = array('class' => 'btn btn-success');
						 			echo form_submit('btnSubmit','Guardar', $claseBtnSubmit);
						 		 ?>
						 	</div>
						 	<div class="col-6 text-left">
						 		<?php 
						 			$claseBtnReset = array('class' => 'btn btn-dark' );
						 			echo form_submit('btnReset','Restablecer', $claseBtnReset);
						 		 ?>
						 	</div>
						 </div>
						 <?php echo form_close(); ?>
					</div>
					<div id="messageSave">
							<?php 
								if(!empty($messageSave)){
									echo $messageSave;
								}
							?>
					</div>

					<div id="error">
							<?php
								echo validation_errors();
							?>
					</div>


				<div id="messageDelete">
					<?php 
						if (!empty($messageDelete)) {
							echo $messageDelete;
						}
					 ?>
				</div>
				
			</div>
			<div class="container-fluid">
				<?php 
					if (!empty($resultQuery)) {
						?>
						
						<div class="tableBd">
							<div class="row border border-dark">
								<div class="col-6 text-right">
									<b>Foto</b>
								</div>
								<div class="col-6 text-left">
									<b>Datos</b>
								</div>
							</div>
							
							<?php 
								foreach ($resultQuery->result() as $row) {
									?>
									
									<div class="container caja">
										<div class="row">
											<div class="col-6 text-right">
												<img src="<?php echo base_url().'assets/images/'. $row->foto?>" alt="" class="imgMia2">
											</div>
											<div class="col-6 text-left">
												<div class="row">
													<div class="col-12 text-center">
														<?php echo $row->id; ?>
													</div>
												</div>
												<div class="row">
													<div class="col-12 text-center">
														<?php echo $row->usuario; ?>
													</div>
												</div>
												<div class="row">
													<div class="col-12 text-center">
														<?php echo $row->nombre; ?>
													</div>
												</div>
												<div class="row">
													<div class="col-12 text-center">
														<?php echo $row->correo; ?>
													</div>
												</div>
												<div class="row">
													<div class="col-12 text-center">
														<?php echo $row->fecha_hora_registro; ?>
													</div>
												</div>
												<div class="row">
													<div class="col-6 text-right">
														<a href="<?php echo base_url().'index.php/Start/delete/usuario/'.$row->id?>" title="Eliminar a <?php echo $row->nombre?>" class="btn btn-danger" onclick="return confirm('¿Estas seguro?');">Eliminar</a>
													</div>
													<div class="col-6">
														<a href="<?php echo base_url().'index.php/Start/find/usuario/id/'.$row->id?>" title="Modificar a <?php echo $row->nombre?>" class="btn btn-primary" onclick="return confirm('¿Estas seguro?');">Modificar</a>
													</div>
												</div>
											</div>
										</div>
									</div>

								<?php
								}
							 ?>
							
							<div class="row">
								<div class="col-12 text-center">
									Total Registros: <?php echo $totalRows; ?>
								</div>
							</div>
						</div>
				<?php

					}
				 ?>
			</div>
			<div id="dialog" title="Mensaje del sistema">
				
			</div>
		</div>
		
        <!-- Llamados a los scripts -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="<?php echo base_url()."assets/js/main.js"?>"></script>
    </body>
</html>