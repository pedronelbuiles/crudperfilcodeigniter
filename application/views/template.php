<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>view - Users</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php base_url() ?>">
    </head>
    <body>
		<div class="container">
			<div class="header text-center">
				<h1>Titulo</h1>
			</div>
		</div>
		<div class="container">
			<div class="row border border-dark">
				<div class="col-6 text-right">
					<b>Foto</b>
				</div>
				<div class="col-6">
					<<b>Datos</b>
				</div>
			</div>
			<div class="row">
				<div class="col-6 text-right">
					<!-- img -->
				</div>
				<div class="col-6 text-left">
					<div class="row">
						<div class="col-12">
							<!-- ID -->
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<!-- usuario -->
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<!-- nombre -->
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<!-- correo -->
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<!-- fecha deregistro -->
						</div>
					</div>
				</div>
			</div>
		</div>
        <!-- Llamados a los scripts -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="js/main.js"></script>
    </body>
</html>