<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class perfil extends CI_Controller {

	/**
	 * Controlador para maejar el CRUD de la aplicación de usuario con CodeIgniter
	 */

    private $arrayData;
    public function __construct()
	{
		parent::__construct();
		$this->load->model('modelPerfil');
	}

	public function __destruct()
	{
		
	}

	public function index()
	{
		
    }
    public function read()
	{

		$pagination=2;
		$config['base_url']=  base_url()."index.php/perfil/read";
		$config['total_rows']=$this->modelPerfil->totalRecords('perfil');
		$config['num_links']=1;
		$config['per_page']=$pagination;
		$config['first_link']="<< primero";
		$config['prev_link']="< anterior";
		$config['next_link']="siguiente >";
		$config['last_link']="ultimo >>";
		$config['full_tag-open']="<div id='pagination'>";
		$config['full_tag_close']="</div>";


		$this->pagination->initialize($config);
		$this->arrayData['pagination']= $this->pagination->create_links();
		$fieldsArray = ['*'];
		$this->arrayData['resultQuery'] = $this->modelPerfil->read('perfil', $fieldsArray,$pagination,$this->uri->segment(3));
		$this->arrayData['totalRows'] = $this->modelPerfil->totalRecords('perfil');
		$this->load->view('perfiles', $this->arrayData);
		

		

		
    }
    public function delete($table='', $id=-1)
	{
		$arrayClause = ['id' => $id];
		if ($this->modelPerfil->delete($table, $arrayClause)) {
			$this->arrayData['messageDelete'] = "<b>Registro eliminado</b>";
		}else{
			$this->arrayData['messageDelete'] = "<b>El registro no pudo ser eliminado</b>";
		}
		$this->read();
	}

	public function find($table="", $field="", $value="")
	{
		$arrayRecords = $this->modelPerfil->find($table, $field, $value);
		if ($arrayRecords->num_rows()>0) {
			$this->arrayData['record'] = $arrayRecords;
		}else{
			$this->arrayData['messageUpdate'] = "<i>El registro solicitado no existe en la base de datos</i>";
		}
		$this->read();
	}

	public function save()
	{
		$this->form_validation->set_rules("descripcion", "Descripcion", "trim|required|min_length[3]|max_length[300]");
		
		
		
		
		if ($this->form_validation->run() != FALSE) {
			$arrayRecords = [
				'descripcion' => $this->input->post('descripcion')
			];
			if ($this->input->post('id') == ""){
				$arrayRecord = $this->modelPerfil->find('perfil', 'id', $this->input->post('id'));
				if($arrayRecord->num_rows() > 0){
                    $this->arrayData['messageSave'] = '<i>Este usuario ya se encuentra registrado</i>';
                }else{
					if($this->modelPerfil->insert('perfil', $arrayRecords)){
						$this->arrayData['messageSave'] = '<b>Registro insertado correctamente</b>';
					}else{
						$this->arrayData['messageSave'] = '<b>No se pudo guardar el registro</b>';
					}
				}
			}else {
				if($this->modelPerfil->update('perfil', $arrayRecords, $this->input->post('id'))){
					$this->arrayData['messageSave'] = '<b>Registro actualizado correctamente</b>';
				}else{
					$this->arrayData['messageSave'] = '<b>No se pudo guardar el registro</b>';
				}
			}
		}
		$this->read();
		
	}

}