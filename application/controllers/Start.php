<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Start extends CI_Controller {

	/**
	 * Controlador para maejar el CRUD de la aplicación de usuario con CodeIgniter
	 */

	private $arrayData;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ModelDatabase');
	}

	public function __destruct()
	{
		
	}

	public function index()
	{
		
	}

	public function read()
	{
		$fieldsArray = ['*'];
		$this->arrayData['resultQuery'] = $this->ModelDatabase->read('usuario', $fieldsArray);
		$this->arrayData['totalRows'] = $this->ModelDatabase->totalRecords('usuario');
		$this->load->view('users', $this->arrayData);
	}

	public function delete($table='', $id=-1)
	{
		$arrayClause = ['id' => $id];
		if ($this->ModelDatabase->delete($table, $arrayClause)) {
			$this->arrayData['messageDelete'] = "<b>Registro eliminado</b>";
		}else{
			$this->arrayData['messageDelete'] = "<b>El registro no pudo ser eliminado</b>";
		}
		$this->read();
	}

	public function find($table="", $field="", $value="")
	{
		$arrayRecords = $this->ModelDatabase->find($table, $field, $value);
		if ($arrayRecords->num_rows()>0) {
			$this->arrayData['record'] = $arrayRecords;
		}else{
			$this->arrayData['messageUpdate'] = "<i>El registro solicitado no existe en la base de datos</i>";
		}
		$this->read();
	}

	public function save()
	{
		$this->form_validation->set_rules("usuario", "Usuario", "trim|required|min_length[3]|max_length[100]");
		$this->form_validation->set_rules("nombre", "Nombre", "trim|required|min_length[3]|max_length[100]");
		$this->form_validation->set_rules("correo", "Correo", "trim|required|valid_email|min_length[3]|max_length[100]");
		$this->form_validation->set_rules("clave", "Clave", "trim|required|min_length[3]|max_length[100]|matches[claveConfirm]");
		$this->form_validation->set_rules("claveConfirm", "Confirmar clave", "trim|required|min_length[3]|max_length[100]");
		
		
		if ($this->form_validation->run() != FALSE) {
			$arrayRecords = [
				'usuario' => $this->input->post('usuario'),
				'nombre' => $this->input->post('nombre'),
				'correo' => $this->input->post('correo'),
				'clave' => $this->input->post('clave'),
				'foto' => $this->input->post('foto')
			];
			if ($this->input->post('id') == ""){
				$arrayRecord = $this->ModelDatabase->find('usuario', 'usuario', $this->input->post('usuario'));
				if($arrayRecord->num_rows() > 0){
                    $this->arrayData['messageSave'] = '<i>Este usuario ya se encuentra registrado</i>';
                }else{
					if($this->ModelDatabase->insert('usuario', $arrayRecords)){
						$this->arrayData['messageSave'] = '<b>Registro insertado correctamente</b>';
					}else{
						$this->arrayData['messageSave'] = '<b>No se pudo guardar el registro</b>';
					}
				}
			}else {
				if($this->ModelDatabase->update('usuario', $arrayRecords, $this->input->post('id'))){
					$this->arrayData['messageSave'] = '<b>Registro actualizado correctamente</b>';
				}else{
					$this->arrayData['messageSave'] = '<b>No se pudo guardar el registro</b>';
				}
			}
		}
		$this->read();
		
	}


}
