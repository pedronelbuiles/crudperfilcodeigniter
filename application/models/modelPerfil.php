<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Modelo para realizar consultas a la base de datos
 */
class modelPerfil extends CI_Model{

    public function __construct()
	{
		parent::__construct();
	}

	public function __destruct()
	{
		
	}

	public function index()
	{
		
    }

    public function read($table, $fieldsArray)
	{
		$this->db->select($fieldsArray);
		$this->db->from($table);
		return $this->db->get();
		//Para una consulta en lenguaje sql utilizamos el método query()
		//$cadena_consulta = 'CONSULTA EN SQL';
		//$this->db->query($cadena_consulta);
	}
	public function totalRecords($table)
	{
		return $this->db->get($table)->num_rows();
	}

	public function delete($table, $arrayClause)
	{
		return $this->db->delete($table, $arrayClause);
	}

	public function find($table, $field, $value)
	{
		$this->db->where("UPPER($field)", strtoupper($value));
		return $this->db->get($table);
	}

	public function update($table, $record, $id)
	{
		$this->db->where('id', $id);
		return $this->db->update($table, $record);
	}

	public function insert($table, $record)
	{
		return $this->db->insert($table, $record);
	}
    
}